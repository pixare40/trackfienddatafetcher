# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field


class TrackFiendNewsItem(Item):
    title = Field()
    summary = Field()
    news_story = Field()
    date_created = Field()
    news_item_image = Field()
    date_fetched = Field()
    url_to_story = Field()
    news_item_main_artist = Field()
    news_item_supporting_artists = Field()
    item_type = Field()
    tags = Field()
    source = Field()

class TrackFiendArtistItem(Item):
    artist_name = Field()
    artist_cname = Field()
    artist_picture = Field()
    given_name = Field()
    home_town = Field()
    label = Field()
    facebook = Field()
    twitter = Field()
    instagram = Field()
    url_to_artist = Field()
    item_type = Field()
