# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
import psycopg2

class MongoPipeline(object):

    news_collection_name = "news"
    artists_collection_name = "artist"

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE')
            )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def store_news_item(self, item):

        news_item_name = item['title']
        if(self.db[self.collection_name].find({'title': news_item_name}).count() > 0):
            print "item in db"
            return item
        else:
            self.db[self.news_collection_name].insert(dict(item))
            return item

    def store_artist_item(self, item):

        artist_name = item['artist_name']
        print artist_name
        if(self. db[self.artists_collection_name].find({'artist_name': artist_name}).count() > 0):
            print "artist already in db"
            return item
        else:
            self.db[self.artists_collection_name].insert(dict(item))
            return item

    def process_item(self, item, spider):

        item_type = item['item_type']
        if(item_type == "artist"):
            return self.store_artist_item(item)
        else:
            return self.store_news_item(item)
