# -*- coding: utf-8 -*-
import scrapy
import lxml
from scrapy.selector import Selector
import datetime
from TrackFiendDataFetch.items import TrackFiendNewsItem

class HotNewHipHopSpider(scrapy.Spider):
    name = "hot_new_hip_hop"
    allowed_domains = ["hotnewhiphop.com"]
    start_urls = (
        'http://www.hotnewhiphop.com/songs/',
    )

    def parse(self, response):

        sel = Selector(response)
        songs = sel.xpath('//div[@class="grid-item song"]')

        for song in songs:
            title = song.xpath('string(.//a[@class="cover-title grid-item-title"])').extract_first()
            summary = song.xpath('string(.//span[@class="song-review"])').extract_first()
            date_created = song.xpath('string(.//span[@class="js-live-date field-processed--js-live-date"]/@data-date)').extract_first()
            news_item_image = song.xpath('string(.//img/@src)').extract_first()
            url_to_story = song.xpath('string(.//a[@class="cover-anchor"]/@href)').extract_first()
            news_item_main_artist = song.xpath('string(.//em[@class="default-artist"])').extract_first()
            current_datetime = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")


            item = TrackFiendNewsItem()
            item['title'] = title.strip()
            item['summary'] = summary.strip()
            item['news_item_image'] = news_item_image.strip()
            item['date_fetched'] = current_datetime
            item['url_to_story'] = 'http://www.hotnewhiphop.com' + url_to_story.strip()
            item['news_item_main_artist'] = news_item_main_artist.strip()
            item['item_type'] = 'song'
            item['tags'] = news_item_main_artist.strip()
            item['source'] = 'HotNewHipHop'

            print item
            yield item

