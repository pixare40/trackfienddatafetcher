# -*- coding: utf-8 -*-
import scrapy


class HotNewHipHopNewsSpider(scrapy.Spider):
    name = "hot_new_hip_hop_news"
    allowed_domains = ["hotnewhiphop.com"]
    start_urls = (
        'http://www.hotnewhiphop.com/',
    )

    def parse(self, response):
        pass
