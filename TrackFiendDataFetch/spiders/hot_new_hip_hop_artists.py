# -*- coding: utf-8 -*-

import sys
import scrapy
import codecs
import datetime
import lxml.html

from scrapy.selector import Selector
from TrackFiendDataFetch.items import TrackFiendArtistItem

class HotNewHipHopArtistsSpider(scrapy.Spider):

    name = "hot_new_hip_hop_artists"
    allowed_domains = ["hotnewhiphop.com"]
    base_url_to_start = 'http://www.hotnewhiphop.com'
    start_urls = (
        'http://www.hotnewhiphop.com/artists',
    )

    def parse(self, response):

        sel = Selector(response)
        artists = sel.xpath('//div[@class="grid-item artist"]')

        for artist in artists:

            artist_item = TrackFiendArtistItem()

            relative_url_to_artist =  artist.xpath('string(.//a[@class="cover-anchor artist-cover-anchor"]/@href)').extract_first()
            url_to_artist = self.base_url_to_start + relative_url_to_artist.strip();
            artist_name = artist.xpath('string(.//a[@class="cover-anchor artist-cover-anchor"]/@title)').extract_first()

            artist_item['url_to_artist'] = url_to_artist
            artist_item['artist_name'] = artist_name
            artist_item['artist_cname'] = self.get_artist_cname(artist_name)

            request = scrapy.Request(url=url_to_artist, callback= self.parse_artist_article)
            request.meta['item'] = artist_item

            yield request

        next_page = sel.xpath('string(//a[@class="page-link next-page"]/@href)').extract_first()

        if next_page:
            next_page_url = self.base_url_to_start + next_page.strip()
            next_page_parse_request = scrapy.Request(url=next_page_url, callback=self.parse)

            yield next_page_parse_request

    def parse_artist_article(self, response):

        artist_item = response.meta['item']
        
        sel = Selector(response)

        given_name = sel.xpath('string(//div[@class="artist-infoTable"]/table/tr[1]/td/span)').extract_first()
        home_town = sel.xpath('string(//div[@class="artist-infoTable"]/table/tr[4]/td/span)').extract_first()
        artist_pic = sel.xpath('string(//div[@class="simplePix-slide js-simplePix-slide  active"]/img/@src)').extract_first()

        artist_item['item_type'] = 'artist'
        artist_item['given_name'] = given_name
        artist_item['home_town'] = home_town
        artist_item['artist_picture'] = artist_pic

        yield artist_item

    def get_artist_cname(self, artist_name=""):

        stripped_name = artist_name.strip()
        replace_spaces = artist_name.replace(" ","_")

        return replace_spaces.lower()
